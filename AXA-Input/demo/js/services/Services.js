app.service('Services', function ($rootScope, $http) {
	

	this.getExamples = function(success, fault){
		this.callService('demo/data/Examples.json', null,  success, fault);
	}
	
	 this.callService = function(uri, headers, success, fault, serviceMethod, theData){
        var isNoHeader = headers == "NOHEADER";
        if(!serviceMethod)serviceMethod = "GET";
        if(!headers || isNoHeader){
            headers = {};
        }
        if(isNoHeader)headers = null;

        var theThis = this;

        var req = {
            method: serviceMethod,
            url: uri,
            headers: headers,
            data : theData
        }

        $http(req).success(function(data, status, headers, config){
            theThis.onSuccess(data, status, headers, config, success);
        }).error(function(errorData, errorStatus, headers, config)
        {
            theThis.onFault(errorData, errorStatus, headers, config, fault);
        });
    }

    this.onSuccess = function(data, status, headers, config, success){
        if(!success)return;
        if(data && data.Error && data.Error.Message)console.log(data.Error.message);
        if(data && data.Warn && data.Warn.Message)console.log(data.Warn.message);
        success(data, status, headers, config);
    }

    this.onFault = function(errorData, errorStatus, headers, config, fault){
        if(!fault)return;
        fault(errorData, errorStatus, headers, config, fault);
    }
});

