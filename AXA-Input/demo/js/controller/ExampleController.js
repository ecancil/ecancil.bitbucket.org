app.controller('ExampleController', function ($scope, $timeout, Services, $sce, $mdToast, $timeout){
	
	$scope.examples;
	$scope.search = {};
	$scope.reasons = [];
	$scope.countries = [];
	$scope.flag = {};
	$scope.selectedInputCountry;
	
	

	$scope.$watch("selectedInput", function(val){
		if(val == null)return;
		$scope.filterText = val;
		if($scope.examples)$.each($scope.examples, function(index, value){
			if(value.title == val){
				$scope.selectedInputCountry = value.country;
			}
		});
		$scope.reset();
	});
	
	$scope.$watch("filterText", function(val){
		$scope.filterText = val;
		$scope.reset();
		var doesMatch = false;
		if($scope.examples)for(var i = 0; i < $scope.examples.length; i ++){
			var ex = $scope.examples[i];
			if(ex.title == val)doesMatch = true;
		}
		
		if(!doesMatch)$scope.selectedInput = null;
	});
	
	$scope.$watch("flag.selectedFlag", function(val){
		$scope.setSelectedInput();
	});
	

	
	
	$scope.setSelectedInput = function(){
		$scope.selectedInput = "";
		$scope.selectedInputCountry = null;
	}
	
	
	Services.getExamples(function(evt){
		$scope.examples = evt.examples;
		for(var index in evt.examples){
			var example = evt.examples[index];
			var html = example.html;
			example.originalHtml = html;
			var trusted = $sce.trustAsHtml(html);
			example.html = trusted;
			if(example.country){
				if($scope.countries.indexOf(example.country) < 0)$scope.countries.push(example.country);
			}else{
				example.country = "global";
			}
			
			
			
		}
		$scope.search.examples = $scope.examples;
	},
	function(err){
		var test;
	});
	
	$scope.showActionToast = function() {
		$mdToast.show(
	      $mdToast.simple()
	        .textContent('Copied Successfully!')
	        .position("top right")
	        .hideDelay(3000)
	    );
	}
	
	$scope.reset = function(){
		for(var index in $scope.examples){
			$scope.examples[index].type = null;
		}
	}
	
	 $.fn.inputEval.config.passcallback = function (input, oldVal, newVal, reason){
		 getErDone();
	}
	
	$.fn.inputEval.config.failcallback = function(input, reason){
		getErDone();
	}
	
	var count = 0;
	$.fn.inputEval.config.forminvalidatedcallback = function(invalidatedForm, invalidatedFields){
		try{
			$scope.isValid = false;
			getErDone();
			$timeout(function(){$scope.$apply();},1);
		}catch(err){
			var test;
		}
	}
	
	$.fn.inputEval.config.formvalidatedcallback = function(validatedForm){
		try{
			$scope.isValid = true;
			getErDone();
			$timeout(function(){$scope.$apply();},1);
		}catch(err){
			
		}
	}
	
	function getErDone(){
		$scope.reasons = [];
		//shouldnt be doing this in the controller, but whatever dont judge me
		var elements = $.fn.getInvalidFormElements($("#theForm")).reverse();
		$.each(elements, function(index, item){
			var reason = {};
			var val = "";
			val += item.attr("id") + " reason(s):" + $.fn.getFormElementInvalidReasons(item).join(" ");
			reason.reason = val;
			reason.input = "#"+item.attr("id");
			$scope.reasons.push(reason);
		});
	}
	
});
