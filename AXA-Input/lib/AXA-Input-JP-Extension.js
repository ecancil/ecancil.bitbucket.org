(function($) {
    var HKANA = new Array(
        "ｶﾞ", "ｷﾞ", "ｸﾞ", "ｹﾞ", "ｺﾞ", "ｻﾞ", "ｼﾞ", "ｽﾞ", "ｾﾞ", "ｿﾞ",
        "ﾀﾞ", "ﾁﾞ", "ﾂﾞ", "ﾃﾞ", "ﾄﾞ", "ﾊﾞ", "ﾋﾞ", "ﾌﾞ", "ﾍﾞ", "ﾎﾞ", "ｳﾞ", //濁音
        "ﾊﾟ", "ﾋﾟ", "ﾌﾟ", "ﾍﾟ", "ﾎﾟ", //半濁音
        "ｧ", "ｨ", "ｩ", "ｪ", "ｫ", "ｬ", "ｭ", "ｮ", "ｯ", "ｰ", "", // 小文字
        "ｱ", "ｲ", "ｳ", "ｴ", "ｵ", "ｶ", "ｷ", "ｸ", "ｹ", "ｺ", // 50音
        "ｻ", "ｼ", "ｽ", "ｾ", "ｿ", "ﾀ", "ﾁ", "ﾂ", "ﾃ", "ﾄ",
        "ﾅ", "ﾆ", "ﾇ", "ﾈ", "ﾉ", "ﾊ", "ﾋ", "ﾌ", "ﾍ", "ﾎ",
        "ﾏ", "ﾐ", "ﾑ", "ﾒ", "ﾓ", "ﾔ", "ﾕ", "ﾖ",
        "ﾗ", "ﾘ", "ﾙ", "ﾚ", "ﾛ", "ﾜ", "", "ｦ", "", "ﾝ" // 50音 end
    );

    var WKANA = new Array( // _genCharLinesでは、半角カナとマッピングがずれる。
        "ガ", "ギ", "グ", "ゲ", "ゴ", "ザ", "ジ", "ズ", "ゼ", "ゾ",
        "ダ", "ヂ", "ヅ", "デ", "ド", "バ", "ビ", "ブ", "ベ", "ボ", "ヴ", //濁音
        "パ", "ピ", "プ", "ペ", "ポ", //半濁音
        "ァ", "ィ", "ゥ", "ェ", "ォ", "ャ", "ュ", "ョ", "ッ", "ー", "ヮ", // 小文字
        "ア", "イ", "ウ", "エ", "オ", "カ", "キ", "ク", "ケ", "コ", // 50音 start
        "サ", "シ", "ス", "セ", "ソ", "タ", "チ", "ツ", "テ", "ト",
        "ナ", "ニ", "ヌ", "ネ", "ノ", "ハ", "ヒ", "フ", "ヘ", "ホ",
        "マ", "ミ", "ム", "メ", "モ", "ヤ", "ユ", "ヨ",
        "ラ", "リ", "ル", "レ", "ロ", "ワ", "ヰ", "ヲ", "ヱ", "ン" // 50音 end
    );

    $.fn.FWKanaToHWKana = function(config) {
        var input = $(this);
        //no specific local configs for this
        //super config can be updated via node or config param
        config = $.fn.getLocalConfig(input, config, $.fn.inputEval.INPUT_TYPE_ENUM.FW_KANA_TO_HW_KANA);
        var newString = kanaToKana(input, WKANA, HKANA);
        $.fn.inputEval(input, newString !== null, function() {
            return newString;
        }, null, config, false, $.fn.inputEval.INPUT_TYPE_ENUM.FW_KANA_TO_HW_KANA);
        return input;
    }

    $.fn.HWKanaToFWKana = function(config) {
        var input = $(this);
        //no specific local configs for this
        //super config can be updated via node or config param
        config = $.fn.getLocalConfig(input, config, $.fn.inputEval.INPUT_TYPE_ENUM.HW_KANA_TO_FW_KANA);
        var newString = kanaToKana(input, HKANA, WKANA);
        $.fn.inputEval(input, newString !== null, function() {
            return newString;
        }, null, config, false, $.fn.inputEval.INPUT_TYPE_ENUM.HW_KANA_TO_FW_KANA);
        return input;
    }

    //reduce duplication by passing two previous function calls through this method
    function kanaToKana(input, from, to) {
        var has = false;
        var newString = "";
        var j = 0;
        for (j; j < input.val().length; j++) {
            var toAppend = input.val().substr(j, 1);
            if ($.inArray(toAppend, from) > -1) {
                toAppend = to[$.inArray(toAppend, from)];
                has = true;
            }
            newString += toAppend;
        }
        return has ? newString : null;
    }

    //can pass removeerrantcharacters = false in config and they wont be removed - by default its true
    $.fn.onlyKanji = function(config) {
        var input = $(this);
        var localConfig = {
            removeerrantcharacters: true
        };
        config = $.extend(localConfig, config);
        if (input.attr('removeerrantcharacters')) {
            config.removeerrantcharacters = $.fn.localEval(input.attr('removeerrantcharacters'));
        }
        if (config.removeerrantcharacters === null) delete config.removeerrantcharacters;
        if (config.itemtofocuswhenmaxlengthreached === null) delete config.itemtofocuswhenmaxlengthreached;
        config = $.fn.getLocalConfig(input, config, $.fn.inputEval.INPUT_TYPE_ENUM.ONLY_KANJI);
        var val = input.val();
        var cleaned = val.replace(/(?![\u4E00-\u9FAF])./g, '');
        if (val != cleaned && config.removeerrantcharacters) {
            //remove all non kanji characters
            $.fn.inputEval(input, input.val().length > 0, function() {
                return cleaned;
            }, null, config, false, $.fn.inputEval.INPUT_TYPE_ENUM.ONLY_KANJI);
        } else {
            //dont remove the kanji characters, however now this field becomes one that causes invalidation of the form
            $.fn.inputEval(input, val == cleaned && val != null && val !== '', function() {
                return val
            }, null, config, !config.removeerrantcharacters, $.fn.inputEval.INPUT_TYPE_ENUM.ONLY_KANJI);
        }
        return input;
    }

    //can pass removeerrantcharacters = false in config and they wont be removed - by default its true
    $.fn.onlyKana = function(config) {
        var input = $(this);
        var localConfig = {
            removeerrantcharacters: true
        };
        config = $.extend(localConfig, config);
        if (input.attr('removeerrantcharacters')) {
            config.removeerrantcharacters = $.fn.localEval(input.attr('removeerrantcharacters'));
        }
        if (config.removeerrantcharacters === null) delete config.removeerrantcharacters;
        config = $.fn.getLocalConfig(input, config, $.fn.inputEval.INPUT_TYPE_ENUM.ONLY_KANA);
        var val = input.val();
        var cleaned = val.replace(/(?![\u3040-\u309f]|[\u30A0-\u30FF])./g, '');
        if (val != cleaned && config.removeerrantcharacters) {
            //remove all non kanji characters
            $.fn.inputEval(input, input.val().length > 0, function() {
                return cleaned;
            }, null, config, false, $.fn.inputEval.INPUT_TYPE_ENUM.ONLY_KANA);
        } else {
            //dont remove the kanji characters, however now this field becomes one that causes invalidation of the form
            $.fn.inputEval(input, val == cleaned && val !== null && val !== '', function() {
                return val;
            }, null, config, !config.removeerrantcharacters, $.fn.inputEval.INPUT_TYPE_ENUM.ONLY_KANA);
        }
        return input;
    }

    $.fn.onlyHalfWidthCharacters = function(config) {
        var input = $(this);
        config = $.fn.getLocalConfig(input, config, $.fn.inputEval.ONLY_HALF_WIDTH_CHARACTERS);
        var val = input.val();
        var cleaned = val.replace(/(?![\uFF60-\uFFEF])./g, '');
        $.fn.inputEval(input, input.val().length > 0, function() {
            return cleaned;
        }, null, config, false, $.fn.inputEval.INPUT_TYPE_ENUM.ONLY_HALF_WIDTH_CHARACTERS);
        return input;
    }

    $.fn.onlyFullWidthCharacters = function(config) {
        var input = $(this);
        config = $.fn.getLocalConfig(input, config, $.fn.inputEval.ONLY_FULL_WIDTH_CHARACTERS);
        var val = input.val();
        var cleaned = val.replace(/(?![\uFF00-\uFF60]|[\u3040-\u309F]|[\u30A0-\u30FF])./g, '');
        $.fn.inputEval(input, input.val().length > 0, function() {
            return cleaned;
        }, null, config, false, $.fn.inputEval.INPUT_TYPE_ENUM.ONLY_FULL_WIDTH_CHARACTERS);
        return input;
    }

    $.fn.inputEval.INPUT_TYPE_ENUM.FW_KANA_TO_HW_KANA = "fwkanatohwkana";
    $.fn.inputEval.INPUT_TYPE_ENUM.HW_KANA_TO_FW_KANA = "hwkanatofwkana";
    $.fn.inputEval.ONLY_KANJI = "onlykanji";
    $.fn.inputEval.ONLY_KANA = "onlykana";
    $.fn.inputEval.ONLY_HALF_WIDTH_CHARACTERS = "onlyhalfwidthcharacters";
    $.fn.inputEval.ONLY_FULL_WIDTH_CHARACTERS = "onlyfullwidthcharacters";

    $.fn.registerValidationFunction($.fn.inputEval.INPUT_TYPE_ENUM.FW_KANA_TO_HW_KANA, $.fn.FWKanaToHWKana);
    $.fn.registerValidationFunction($.fn.inputEval.INPUT_TYPE_ENUM.HW_KANA_TO_FW_KANA, $.fn.HWKanaToFWKana);
    $.fn.registerValidationFunction($.fn.inputEval.INPUT_TYPE_ENUM.ONLY_KANJI, $.fn.onlyKanji);
    $.fn.registerValidationFunction($.fn.inputEval.INPUT_TYPE_ENUM.ONLY_KANA, $.fn.onlyKana);
    $.fn.registerValidationFunction($.fn.inputEval.INPUT_TYPE_ENUM.ONLY_HALF_WIDTH_CHARACTERS, $.fn.onlyHalfWidthCharacters);
    $.fn.registerValidationFunction($.fn.inputEval.INPUT_TYPE_ENUM.ONLY_FULL_WIDTH_CHARACTERS, $.fn.onlyFullWidthCharacters);

})(jQuery);